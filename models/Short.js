const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ShortSchema = new Schema({
  originalLink: {
    type: String,
    required: true
  },
  shortLink: {
    type: String,
    required: true,
    unique: true
  }
});

const Short = mongoose.model('Short', ShortSchema);

module.exports = Short;