const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const short = require('./app/short');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect('mongodb://localhost/shorter', {useNewUrlParser: true}).then(() => {
    app.use('/short', short);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});