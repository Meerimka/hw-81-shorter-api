const express = require('express');
const nanoid = require('nanoid');
const Short= require('../models/Short');

const router = express.Router();

router.get('/:shortLink', (req, res) => {
    Short.findOne({shortLink: req.params.shortLink})
        .then(originalLink => res.status(301).redirect(originalLink.originalLink))
        .catch(() => res.sendStatus(500));
});



router.post('/', (req, res) => {
    const shortData = req.body;

    shortData.shortLink = nanoid(7);

    const short = new Short(shortData);

    short.save()
        .then(result => res.send(result.shortLink))
        .catch(error => res.status(400).send(error));
});

module.exports = router;
